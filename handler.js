const { ServerClient: PostmarkClient } = require('postmark')
const postmark = new PostmarkClient(process.env.POSTMARK_API_TOKEN)

module.exports.postmark_email = async (event, context) => {
    if (
		(await event.headers['x-moltin-secret-key']) !=
		process.env.MT_WEBHOOK_SECRET
	  ) {
		return {
			statusCode: 403,
			body: "Forbidden: You don't have permission to access this feature."
        };
    }
    const response = {
        headers: { 'Access-Control-Allow-Origin': '*' }, // CORS requirement
        statusCode: 200,
    };

    var message;

	// Now we try to parse the incoming request's body and build a message JSON
	try {
        // Parses the incoming request body
        const resource = event.body.payload;
        console.log("Resource Payload:", resource);
		const {
            data: {
            id,
            customer: { email: to, name },
            meta: {
                display_price: {
                with_tax: { formatted: order_total },
                },
            },
            },
            included: { items },
        } = resource

		 // Build the message object we'll send to Postmark
   		message = {
            from: process.env.POSTMARK_FROM_ADDRESS,
            to,
            templateId: process.env.POSTMARK_CONFIRMATION_TEMPLATE_ID,
            templateModel: {
            customer_name: name,
            order_total,
            order_items: items,
            },
        };
	} catch (errors) {
        console.log("Message: ", message);
        // If we can't parse it, return an error message.
        console.log("Error processing request: ", errors);
        return {
            statusCode: 400,
            body: event.body
        };
	}

    // Now we try to send the email using Postmark
	try {
		await postmark.sendEmailWithTemplate(message);

		// If everything works, we return a success message
        response.body = JSON.stringify({Message: "Email sent successfully."});
	    return response;
    } catch (errors) {
        // Otherwise we log and return an error message
        console.log("Error sending email for request: ", errors);
        return {
            statusCode: 500,
            body: event.body
        };
    }
};
